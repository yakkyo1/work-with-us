# Yakkyofy Job Application - Backend

We're looking for an experienced NodeJS developer that will join our product team working remotely.

## Overview

Yakkyo SRL is a startup that develops innovative solutions for the dropshipping world. We are currently looking for an experienced Software Developer with a specialization on back-end Application Development, which will join the team by working remotely. You will be part of a multi-lingual, international team and contribute to the next level eCommerce solutions.

## Requirements

- Good knowledge of MongoDB, familiarity with Mongoose;
- Familiarity with Git;
- An elementary acquaintance of Docker containers and images;
- Problem-solving skills, ability to work in a team;
- Basic English communication skills;
- A strong desire to grow within the company.

## What you'll do

- Code microservices with NodeJS using TypeScript along with several modern libraries;
- Develop RESTful APIs with the Express framework;
- Design MongoDB models and schemas with the help of Mongoose;
- Integrations with the major marketplaces' APIs (1688, Alibaba, Amazon, etc);
- Develop Unit tests with Mocha and Chai;
- Integrations with modern eCommerce platforms, such as Shopify, Woocommerce, eBay, and more;
- Manage queues on RabbitMQ;
- Continous delivery to AWS and/or Alibaba Cloud;
- Collaborate with your friendly and enthusiastic teammates.

## Future Developments

- Computer vision algorithm to find similar products on different marketplaces with OpenCV and Flann feature matching;
- Deep learning algorithm with Pytorch or Fastai to predict market trends and find potential "winning" products;
- Scraping data with Puppeteer and Cheerio;
- Elastic search database to speed up researches;
- Move from docker containers to kubernates cloud-native;
- AR viewer to show the product preview in the real world.

## How to Apply

If you are interested to be part of the Yakkyo’s team, please send us your resume in PDF or Word to [info@yakkyofy.com](mailto:info@yakkyofy.com) writing in the object of the email your name and the name of the position you are applying for.
